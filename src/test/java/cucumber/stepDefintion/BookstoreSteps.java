package cucumber.stepDefintion;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.cucumber.datatable.DataTable;
import io.cucumber.guice.ScenarioScoped;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@ScenarioScoped
public class BookstoreSteps {

    @Inject @Named("bookstore-books") private JSONArray books;
    @Inject @Named("bookstore-book") private JSONObject selected;

    @Given("I have taken a bunch of books")
    public void iHaveTakenABunchOfBooks() throws IOException, InterruptedException {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().
                uri(URI.create("https://bookstore.toolsqa.com/BookStore/v1/Books")).
                GET().
                build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        Assert.assertEquals(200, response.statusCode());
        JSONObject body = (JSONObject) JSONValue.parse(response.body());
        books = (JSONArray) JSONValue.parse(body.get("books").toString());
    }

    @And("I think of my topics")
    public void iThinkOfMyTopics(DataTable topics) {
        System.out.println("Topics: " + topics.asList());
    }

    @When("I select book number {int}")
    public void iSelectBookNumber(int bookIndex) {
        selected = (JSONObject) JSONValue.parse(books.get(bookIndex).toString());
    }

    @Then("I should see all its data is displayed")
    public void iShouldSeeAllItsDataIsDisplayed() {
        System.out.println("isbn: " + selected.get("isbn"));
        System.out.println("description: " + selected.get("description"));
    }
}
