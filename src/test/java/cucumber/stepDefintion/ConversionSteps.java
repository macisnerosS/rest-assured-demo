package cucumber.stepDefintion;

import io.cucumber.guice.ScenarioScoped;
import io.cucumber.java.DataTableType;
import io.cucumber.java.DocStringType;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import pojo.cucumber.Book;

import java.util.List;
import java.util.Map;

@ScenarioScoped
public class ConversionSteps {

    private Book anyBook;
    private List<Book> listOfBooks;
    private JSONObject jsonBook;

    // This is the implementation that converts string to Book.class
    @ParameterType(".*")
    public Book book(String name) {
        return new Book(name, null, null, null);
    }

    // This is the implementation that converts each row of a table to Book.class
    @DataTableType
    public Book convertRowToBook(Map<String, String> entryRow) {
        return new Book(
                entryRow.get("title"),
                entryRow.get("topic"),
                entryRow.get("pages"),
                entryRow.get("price")
        );
    }

    // This is the implementation that converts a doc string to Book.class
    @DocStringType
    public Book convertDocStringToBook(String bookInJsonFormat) {
        JSONObject bookJson = (JSONObject) JSONValue.parse(bookInJsonFormat);
        return new Book(
                (String) bookJson.get("title"),
                (String) bookJson.get("topic"),
                (String) bookJson.get("pages"),
                (String) bookJson.get("price")
        );
    }

    // This is the implementation that converts a doc string to JSONObject.class
    @DocStringType
    public JSONObject convertDocStringToJsonNode(String json) {
        return (JSONObject) JSONValue.parse(json);
    }

    @Given("I have a book name")
    public void iHaveABookName() {

    }

    @When("I pass {book} as argument")
    public void iPassAsArgument(Book book) {
        anyBook = book;
    }

    @Then("I should display data of the book instance")
    public void iShouldDisplayDataOfTheBookInstance() {
        System.out.println("-----Book data-----");
        System.out.println("title: " + anyBook.getName());
        System.out.println("topic: " + anyBook.getTopic());
        System.out.println("pages: " + anyBook.getPages());
        System.out.println("price: " + anyBook.getPrice());
    }

    @Given("I have many books name")
    public void iHaveManyBooksName() {

    }

    @When("I pass a table of books")
    public void iPassATableOfBooks(List<Book> books) {
        listOfBooks = books;
    }

    @Then("I should display data of all those books")
    public void iShouldDisplayDataOfAllThoseBooks() {
        for (Book anyBook: listOfBooks) {
            System.out.println("-----Book data-----");
            System.out.println("title: " + anyBook.getName());
            System.out.println("topic: " + anyBook.getTopic());
            System.out.println("pages: " + anyBook.getPages());
            System.out.println("price: " + anyBook.getPrice());
        }
    }

    @Given("I have a docString in mind")
    public void iHaveADocStringInMind() {

    }

    @When("I pass a docstring as json")
    public void iPassADocstringAsJson(Book book) {
        anyBook = book;
    }

    @Then("I should display data of the node instance")
    public void iShouldDisplayDataOfTheNodeInstance() {
        System.out.println("-----Book json data-----");
        System.out.println("title: " + jsonBook.get("title"));
        System.out.println("topic: " + jsonBook.get("topic"));
        System.out.println("pages: " + jsonBook.get("pages"));
        System.out.println("price: " + jsonBook.get("price"));
    }

    @When("I convert a json docstring to jsonNode")
    public void iConvertAJsonDocstringToJsonNode(JSONObject node) {
        jsonBook = node;
    }
}
