Feature: Test diferent types of conversion

  Scenario: Convert String to a class instace

    Given I have a book name
    When I pass "I'm legend" as argument
    Then I should display data of the book instance

  Scenario: Convert each row of a data table to a class instance
    Given I have many books name
    When I pass a table of books
      | title    | topic |pages|price|
      |I'm legend| drama | 233 | 10 |
      |Dr. Zeus  |comedy | 150 |70  |
      |Grinch    |love   |140  |50  |
    Then I should display data of all those books

  Scenario: Convert docString json(or any kind of data you want) to a book(or any kind of data you want)
    Given I have a docString in mind
    When I pass a docstring as json
      """
      {
        "title": "Git Pocket Guide",
        "topic": "drama",
        "pages": "234",
        "price": "10"
      }
      """
    Then I should display data of the book instance

  Scenario: Convert docString json(or any kind of data you want) to a json object (or any kind of data you want)
    Given I have a docString in mind
    When I convert a json docstring to jsonNode
      """
      {
        "title": "Git Pocket Guide",
        "topic": "drama",
        "pages": "234",
        "price": "10"
      }
      """
    Then I should display data of the node instance