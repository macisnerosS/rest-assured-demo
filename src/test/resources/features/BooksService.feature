Feature: Bookstore

  This feature describe the interaction between a client and a user interface
  of a bookstore app.

  Scenario: Select 1 book of your interest
    As a bookstore's client, I want to get a book
    so that I got a bunch of books to select one

    Given I have taken a bunch of books
    When  I select book number 1
    Then I should see all its data is displayed

  Scenario Outline: Select any book of your interest
    As a bookstore's client, I want to get a book
    so that I got a bunch of books to select one

    Given I have taken a bunch of books
    And I think of my topics
      |Horror|
      |Comedy|
      |Drama |
    When  I select book number <chosen_book>
    Then I should see all its data is displayed

    Examples:
      |chosen_book|
      |     5     |
      |     6     |
