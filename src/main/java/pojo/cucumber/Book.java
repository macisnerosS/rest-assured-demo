package pojo.cucumber;

public class Book {

    private String name;
    private String topic;
    private String pages;
    private String price;

    public Book (String name, String topic, String pages, String price) {
        this.name=name;
        this.topic=topic;
        this.pages=pages;
        this.price=price;
    }


    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public String getPages() {
        return pages;
    }

    public String getPrice() {
        return price;
    }
}
