package utils;


import com.google.inject.Guice;
import com.google.inject.Injector;
import io.cucumber.core.backend.ObjectFactory;
import io.cucumber.guice.CucumberModules;
import io.cucumber.guice.ScenarioScope;
import modules.GlobalModule;
import modules.BooksStoreModule;

public final class Factory implements ObjectFactory {

    private Injector injector;

    public Factory() {

        this.injector = Guice.createInjector(
                CucumberModules.createScenarioModule(),
                new GlobalModule(),
                new BooksStoreModule()
        );
    }

    @Override
    public boolean addClass( Class< ? > clazz ) {
        return true;
    }

    @Override
    public void start() {
        this.injector.getInstance(ScenarioScope.class).enterScope();
    }

    @Override
    public void stop() {
        this.injector.getInstance(ScenarioScope.class).exitScope();
    }

    @Override
    public < T > T getInstance( Class< T > clazz ) {
        return this.injector.getInstance( clazz );
    }
}