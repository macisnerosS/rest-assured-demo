package modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class BooksStoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(JSONArray.class).annotatedWith(Names.named("bookstore-books")).toInstance(new JSONArray());
        bind(JSONObject.class).annotatedWith(Names.named("bookstore-book")).toInstance(new JSONObject());
    }
}
