package modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class GlobalModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Names.named("global-driver")).toInstance("Testing");
    }
}
